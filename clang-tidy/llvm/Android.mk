LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libclangTidyLLVMModule
LOCAL_MODULE_CLASS := STATIC_LIBRARIES

include $(LOCAL_PATH)/../Android.common.mk

LOCAL_SRC_FILES := \
  HeaderGuardCheck.cpp \
  IncludeOrderCheck.cpp \
  LLVMTidyModule.cpp \
  TwineLocalCheck.cpp \

LOCAL_STATIC_LIBRARIES := \
  libclangAST \
  libclangASTMatchers \
  libclangBasic \
  libclangLex \
  libclangTidy \
  libclangTidyReadabilityModule \
  libclangTidyUtils \
  libclangTooling \

include $(BUILD_HOST_STATIC_LIBRARY)
