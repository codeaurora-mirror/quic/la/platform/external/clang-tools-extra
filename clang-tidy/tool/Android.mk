LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := clang-tidy
LOCAL_MODULE_CLASS := EXECUTABLES

include $(LOCAL_PATH)/../Android.common.mk

LOCAL_SRC_FILES := \
  ClangTidyMain.cpp \

LOCAL_GROUP_STATIC_LIBRARIES := true

LOCAL_STATIC_LIBRARIES := \
  libLLVMAnalysis \
  libLLVMAsmParser \
  libLLVMBitReader \
  libLLVMCodeGen \
  libLLVMCore \
  libLLVMMC \
  libLLVMMCParser \
  libLLVMOption \
  libLLVMPasses \
  libLLVMProfileData \
  libLLVMScalarOpts \
  libLLVMSupport \
  libLLVMTarget \
  libclangAST \
  libclangASTMatchers \
  libclangAnalysis \
  libclangBasic \
  libclangCodeGen \
  libclangDriver \
  libclangEdit \
  libclangFormat \
  libclangFrontend \
  libclangFrontendTool \
  libclangLex \
  libclangParse \
  libclangRewrite \
  libclangRewriteFrontend \
  libclangSema \
  libclangSerialization \
  libclangStaticAnalyzerCheckers \
  libclangStaticAnalyzerMPIChecker \
  libclangStaticAnalyzerCore \
  libclangStaticAnalyzerFrontend \
  libclangTidy \
  libclangTidyBoost \
  libclangTidyCERTModule \
  libclangTidyCppCoreGuidelinesModule \
  libclangTidyGoogleModule \
  libclangTidyHicppModule \
  libclangTidyLLVMModule \
  libclangTidyMiscModule \
  libclangTidyMPIModule \
  libclangTidyModernizeModule \
  libclangTidyPerformanceModule \
  libclangTidyReadabilityModule \
  libclangTidyUtils \
  libclangTooling \
  libclangToolingCore \

include $(BUILD_HOST_EXECUTABLE)
