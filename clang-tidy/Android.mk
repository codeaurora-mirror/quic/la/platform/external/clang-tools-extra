LOCAL_PATH := $(call my-dir)

# For the host only
# =====================================================
include $(CLEAR_VARS)
include $(CLEAR_TBLGEN_VARS)

LOCAL_MODULE := libclangTidy

LOCAL_MODULE_CLASS := STATIC_LIBRARIES

TBLGEN_TABLES += \
  AttrList.inc \
  AttrVisitor.inc \
  Attrs.inc \
  Checkers.inc \
  CommentCommandList.inc \
  CommentNodes.inc \
  DeclNodes.inc \
  DiagnosticASTKinds.inc \
  DiagnosticCommonKinds.inc \
  DiagnosticFrontendKinds.inc \
  StmtNodes.inc \

LOCAL_SRC_FILES := \
  ClangTidy.cpp \
  ClangTidyModule.cpp \
  ClangTidyDiagnosticConsumer.cpp \
  ClangTidyOptions.cpp \

LOCAL_STATIC_LIBRARIES := \
  libclangAST \
  libclangASTMatchers \
  libclangBasic \
  libclangFormat \
  libclangFrontend \
  libclangLex \
  libclangRewrite \
  libclangSema \
  libclangStaticAnalyzerCore \
  libclangStaticAnalyzerFrontend \
  libclangTooling \
  libclangToolingCore \

LOCAL_LDLIBS_windows := -limagehlp -lversion
LOCAL_LDLIBS_darwin := -lpthread -ldl -lm
LOCAL_LDLIBS_linux := -lpthread -ldl -lm

# remove when we can use PIE binaries in all places again
LOCAL_NO_FPIE := true

include $(CLANG_HOST_BUILD_MK)
include $(CLANG_TBLGEN_RULES_MK)
include $(BUILD_HOST_STATIC_LIBRARY)

subdirs := $(addprefix $(LOCAL_PATH)/,$(addsuffix /Android.mk, \
  boost \
  cert \
  cppcoreguidelines \
  google \
  hicpp \
  llvm \
  misc \
  mpi \
  modernize \
  performance \
  readability \
  tool \
  utils \
  ))

include $(subdirs)
